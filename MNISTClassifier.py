import numpy as np
from PIL import Image
from sklearn import svm
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_openml
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

mnist = fetch_openml('mnist_784')

images = mnist.data.to_numpy()
labels = mnist.target

resized_images = []
new_size = (14, 14)

for image in images:
    img = Image.fromarray(image.reshape(28, 28))
    resized_img = img.resize(new_size, resample=Image.BICUBIC)
    resized_images.append(np.array(resized_img).flatten())

resized_images = np.array(resized_images)

plt.figure(figsize=(10, 10))
for i in range(6):
    plt.subplot(2, 3, i + 1)
    plt.imshow(resized_images[i].reshape(new_size), cmap=plt.cm.gray_r, interpolation='bicubic')
    plt.title("Label: {}".format(labels[i]))

plt.tight_layout()
plt.show()

X, y = resized_images, mnist.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.15, random_state=42)

parameters = {'C': [0.1, 1, 10, 100]}
svc = svm.SVC(kernel='rbf', gamma='scale', class_weight='balanced')

grid_search = GridSearchCV(svc, parameters)
grid_search.fit(X_train, y_train)

best_parameter = grid_search.best_params_
best_model = grid_search.best_estimator_

y_pred = best_model.predict(X_test)

print('Best Parameter:', best_parameter)
print('Accuracy:', accuracy_score(y_test, y_pred))
print('Classification Report:')
print(classification_report(y_test, y_pred))

cm = confusion_matrix(y_test, y_pred)

plt.figure(figsize=(10, 8))
plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
plt.title('Confusion Matrix')
plt.colorbar()
tick_marks = np.arange(10)
plt.xticks(tick_marks, np.arange(10))
plt.yticks(tick_marks, np.arange(10))
plt.xlabel('Predicted Label')
plt.ylabel('True Label')

thresh = cm.max() / 2.0

for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        plt.text(j, i, format(cm[i, j], 'd'), ha='center', va='center',
                 color='white' if cm[i, j] > thresh else 'black')

plt.show()
